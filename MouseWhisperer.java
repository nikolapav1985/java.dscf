import java.awt.event.*;
import javax.swing.*;
/**
* class MouseWhisperer
*
*/
public class MouseWhisperer extends JFrame implements MouseListener {
    MouseWhisperer() {
        super("COME CLOSER"); // make a frame and set title to COME CLOSER
        setSize(300,100); // set size of the frame
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // set in close action, exit on close
        addMouseListener(this); // this object is listening for mouse events
        setVisible(true); // display frame
    }
    public void mouseClicked(MouseEvent e) { setTitle("OUCH"); } // click a button, set frame title OUCH
    public void mousePressed(MouseEvent e) { setTitle("LET GO"); } // keep button pressed, set frame title LET GO
    public void mouseReleased(MouseEvent e) { setTitle("WHEW"); } // button released, set frame title WHEW
    public void mouseEntered(MouseEvent e) { setTitle("I SEE YOU"); } // mouse entered a frame, set frame title I SEE YOU
    public void mouseExited(MouseEvent e) { setTitle("COME CLOSER"); } // mouse exited a frame, set frame title COME CLOSER
    public static void main(String[] args) { new MouseWhisperer(); } // run example
}
